//
//  DetailViewController.h
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodInfo.h"

@interface DetailViewController : UIViewController
@property (nonatomic) FoodInfo *foodInfo;
@end
