//
//  SavedTableViewController.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-07.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "SavedTableViewController.h"
#import "FoodCellTableViewCell.h"
#import "FoodInfo.h"
#import "ComparisonViewController.h"
#import "DetailViewController.h"

@interface SavedTableViewController ()
@property (nonatomic) NSArray *allSavedItems;
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) FoodInfo *selectedItem;
@property (nonatomic) FoodCellTableViewCell *selectedCell;
@property (nonatomic) NSMutableArray *itemsToCompare;
@end

@implementation SavedTableViewController


- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:nil];
    self.selectedItem.hasPicture = YES;
}
-(NSString*)cachePath:(NSString *)foodName {
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = dirs[0];
    NSString *path = [NSString stringWithFormat:@"%@.png",foodName];
    NSString *completePath = [documentDirectory stringByAppendingPathComponent:path];
    self.selectedItem.cachePath = completePath;
    return completePath;
}

-(void)saveImageToCache:(UIImage*)image {
    NSData *data = UIImagePNGRepresentation(self.selectedCell.photo.image);
    BOOL success = [data writeToFile:[self cachePath:self.selectedItem.foodName] atomically:YES];
    if (!success) {
        NSLog(@"Failed to save image to cache");
    }
}

-(void)removeImageFromCache:(NSString*)path {
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    if(error) {
        NSLog(@"removal failed for item at path: %@", path);
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.selectedCell.photo.image = image;
    [self saveImageToCache:image];
    [self updateUserDefaultsWithFoodInfo:self.selectedItem];
    [self.tableView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateUserDefaultsWithFoodInfo:(FoodInfo*)foodInfo {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefault objectForKey:[NSString stringWithFormat:@"allSavedItems"]];
    NSMutableArray *items = [[NSKeyedUnarchiver unarchiveObjectWithData: outData] mutableCopy];
    for (int i = 0; i < items.count; i++) {
        FoodInfo *food = items[i];
        if (food.foodNumber == foodInfo.foodNumber) {
            food.hasPicture = YES;
        }
    }
    NSData *inData = [NSKeyedArchiver archivedDataWithRootObject:items];
    [userDefault setObject:inData forKey:@"allSavedItems"];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefault objectForKey:[NSString stringWithFormat:@"allSavedItems"]];
    self.allSavedItems = [[NSKeyedUnarchiver unarchiveObjectWithData: outData] mutableCopy];
    self.itemsToCompare = [[NSMutableArray alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.allSavedItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodCellTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    
    // Configure the cell...
    FoodInfo *foodInfo = self.allSavedItems[indexPath.row];
    cell.foodName.text = foodInfo.foodName;
    cell.kcalLabel.text = [NSString stringWithFormat:@"%d", foodInfo.energyValue];
    cell.proteinLabel.text = [NSString stringWithFormat:@"%d", foodInfo.proteinValue];
    cell.removeButton.hidden = YES;
    cell.photoButton.hidden = YES;
    cell.compareButton.hidden = YES;
    cell.detailButton.hidden = YES;
    
    if (foodInfo.hasPicture) {
        UIImage *image = [UIImage imageWithContentsOfFile:[self cachePath:foodInfo.foodName]];
        if (image) {
            cell.photo.image = image;
            NSLog(@"picture success");
        } else {
            NSLog(@"Failed to fetch image %@ from file system", foodInfo.cachePath);
        }
    } else {
        cell.photo.image = nil;
    }
    
    if (foodInfo.comparing) {
        cell.compareButton.backgroundColor = [UIColor greenColor];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodInfo *foodInfo = self.allSavedItems[indexPath.row];
    if (foodInfo.hasPicture) {
        return 150;
    } else {
        return 80;
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"foodName contains[c] %@", searchText];
    self.searchResult = [self.allSavedItems filteredArrayUsingPredicate:predicate];
}

- (IBAction)removeItem:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefaults objectForKey:@"allSavedItems"];
    NSMutableArray *items = [[NSKeyedUnarchiver unarchiveObjectWithData: outData] mutableCopy];;
    for (int i = 0; i < items.count; i++) {
        FoodInfo *foodInfo = items[i];
        if (foodInfo.foodNumber == self.selectedItem.foodNumber) {
            [self removeImageFromCache:[self cachePath:foodInfo.foodName]];
            [items removeObjectAtIndex:i];
        }
    }
    self.allSavedItems = items;
    NSData *inData = [NSKeyedArchiver archivedDataWithRootObject:items];
    [userDefaults setObject:inData forKey:@"allSavedItems"];
    [self.tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedItem = self.allSavedItems[indexPath.row];
    self.selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
    [self changeButtonVisibility:NO];
    if (self.selectedItem.comparing) {
        self.selectedCell.compareButton.backgroundColor = [UIColor greenColor];
    }
//    self.selectedCell.removeButton.hidden = NO;
//    self.selectedCell.photoButton.hidden = NO;
//    self.selectedCell.compareButton.hidden = NO;
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
    [self changeButtonVisibility:YES];
//    self.selectedCell.removeButton.hidden = YES;
//    self.selectedCell.photoButton.hidden = YES;
//    self.selectedCell.compareButton.hidden = YES;
}

-(void)changeButtonVisibility:(BOOL)visibility {
    self.selectedCell.removeButton.hidden = visibility;
    self.selectedCell.photoButton.hidden = visibility;
    self.selectedCell.compareButton.hidden = visibility;
    self.selectedCell.detailButton.hidden = visibility;
}

- (IBAction)onCompare:(id)sender {
    if (self.selectedCell.compareButton.backgroundColor == [UIColor greenColor]) {
        self.selectedCell.compareButton.backgroundColor = nil;
        self.selectedItem.comparing = NO;
        [self.itemsToCompare removeObject:self.selectedItem];
    } else {
        if (self.itemsToCompare.count < 2) {
            self.selectedCell.compareButton.backgroundColor = [UIColor greenColor];
            self.selectedItem.comparing = YES;
            [self.itemsToCompare addObject:self.selectedItem];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                               message:@"You already have two items selected for comparison."
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [alert show];
        }
    }
    for (int i = 0; i < self.itemsToCompare.count; i++) {
        FoodInfo *f = self.itemsToCompare[i];
        NSLog(@"%@", f.foodName);
    }
}


- (IBAction)onDetail:(id)sender {
    [self performSegueWithIdentifier:@"segueToDetail" sender:self];
}
- (IBAction)toCompareView:(id)sender {
    if (self.itemsToCompare.count > 1) {
        [self performSegueWithIdentifier:@"segueToComparison" sender:self];
    }
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueToComparison"]) {
        ComparisonViewController *destination = [segue destinationViewController];
        FoodInfo *f1 = self.itemsToCompare[0];
        FoodInfo *f2 = self.itemsToCompare[1];
        NSLog(@"%@ %@", f1.foodName, f2.foodName);
        destination.f1 = self.itemsToCompare[0];
        destination.f2 = self.itemsToCompare[1];
    } else if ([segue.identifier isEqualToString:@"segueToDetail"]) {
        DetailViewController *destination = [segue destinationViewController];
        destination.foodInfo = self.selectedItem;
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
