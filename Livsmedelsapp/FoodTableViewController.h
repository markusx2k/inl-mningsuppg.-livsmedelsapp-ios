//
//  FoodTableViewController.h
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-02-25.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewController : UITableViewController
@property (nonatomic) NSMutableArray *foodItems;
@end
