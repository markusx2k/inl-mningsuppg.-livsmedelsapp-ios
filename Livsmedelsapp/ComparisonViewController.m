//
//  ComparisonViewController.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "ComparisonViewController.h"

@interface ComparisonViewController ()
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;
@property (weak, nonatomic) IBOutlet GKBarGraph *graph;
@end

@implementation ComparisonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.redLabel.text = self.f1.foodName;
    self.blueLabel.text = self.f2.foodName;
    self.graph.dataSource = self;
    self.graph.barWidth = self.graph.frame.size.width/8;
    self.graph.barHeight = self.graph.frame.size.height*0.8;
    self.graph.marginBar = 10;
    
    [self.graph draw];
}


- (NSInteger)numberOfBars {
    return 6;
}
- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    return @[[NSNumber numberWithInteger:(self.f1.energyValue/10)], [NSNumber numberWithInteger:(self.f2.energyValue/10)], [NSNumber numberWithInteger:self.f1.proteinValue], [NSNumber numberWithInteger:self.f2.proteinValue], [NSNumber numberWithInteger:((self.f1.energyValue/100)+self.f1.proteinValue+(self.f1.proteinValue*(self.f1.energyValue)/10))/5], [NSNumber numberWithInteger:((self.f2.energyValue/100)+self.f2.proteinValue+(self.f2.proteinValue*(self.f2.energyValue)/10))/5]][index];
}
- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    if (index % 2 != 0) {
        return [UIColor blueColor];
    } else {
        return [UIColor redColor];
    }
    
    
    //return [UIColor colorWithHue:(float)index  2 saturation:1.0f brightness:1.0f alpha:1.0f];
}
- (UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index {
    //return [UIColor whiteColor];
    return [UIColor colorWithRed:0.1 green:1 blue:0.1 alpha:0.4];
}
- (CFTimeInterval)animationDurationForBarAtIndex:(NSInteger)index {
    return 1.0;
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    return @[@"1.E", @"2.E", @"1.P", @"2.P", @"1.H", @"2.H"][index];
}




@end
