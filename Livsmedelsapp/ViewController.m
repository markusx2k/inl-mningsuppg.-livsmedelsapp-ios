//
//  ViewController.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-02-23.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "ViewController.h"
#import "FoodTableViewController.h"
#import "FoodInfo.h"

@interface ViewController ()
@property (nonatomic) NSMutableArray *foodItems;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIAttachmentBehavior *attatchment;
@property (weak, nonatomic) IBOutlet UIButton *tableButton;
@property (nonatomic) BOOL animatable;
@property (nonatomic) BOOL ready;
@end

@implementation ViewController
- (IBAction)goToList:(id)sender {
    if (self.ready) {
        [self performSegueWithIdentifier:@"segueToTable" sender:self];
    } else {
        NSLog(@"not ready!");
    }
}
- (IBAction)onTap:(UITapGestureRecognizer *)sender {
    if (self.animatable) {
        self.animatable = NO;
        CGPoint tapPos = [sender locationInView:self.view];
        [UIView animateWithDuration:1.0 animations:^{
            self.tableButton.center = tapPos;
        } completion:^(BOOL finished) {
            [self addCustomGravity];
        }];
    }
    
}

-(void)addCustomGravity {
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.tableButton]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.tableButton]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    [self performSelector:@selector(changeAnimationBool) withObject:nil afterDelay:3.0];
}

-(void)changeAnimationBool {
    self.animatable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.foodItems = [[NSMutableArray alloc] init];
    [self loadApi];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidLayoutSubviews {
    [self addCustomGravity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadApi {
    NSURL *url = [NSURL URLWithString:@"http://matapi.se/foodstuff"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {

                                      if (error) {
                                          NSLog(@"Error in response: %@", error);
                                          return;
                                      }
                                      
                                      NSError *parsingError = nil;
                                      NSArray *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                                      NSLog(@"%d", root.count);
                                      
                                      if (!parsingError) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (root.count > 0) {
                                                  for (int i = 0; i < root.count; i++) {
                                                      FoodInfo *foodInfo = [[FoodInfo alloc] init];
                                                      foodInfo.foodName = root[i][@"name"];
                                                      foodInfo.foodNumber = [root[i][@"number"] integerValue];
                                                      [self.foodItems addObject:foodInfo];
                                                  }
                                                  //loop through first 20 items and get kcal and protein
                                                  //save in foodInfo
                                              } else {
                                                  NSLog(@"No topics found"  ) ;
                                              }
                                              NSLog(@"%d", self.foodItems.count);
                                          });
                                      } else {
                                          NSLog(@"Couldn't parse json: %@", parsingError);
                                      }
                                      self.ready = YES;
                                  }];
    [task resume];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueToTable"]) {
        FoodTableViewController *foodTableView = [segue destinationViewController];
        
        foodTableView.foodItems = self.foodItems;
    }
}
    
@end












