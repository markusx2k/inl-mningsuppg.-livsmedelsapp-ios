//
//  FoodTableViewController.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-02-25.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "FoodTableViewController.h"
#import "FoodCellTableViewCell.h"
#import "FoodInfo.h"
#import "SavedTableViewController.h"


@interface FoodTableViewController ()
@property (nonatomic) NSArray *searchResult;
@property (nonatomic) FoodInfo *selectedItem;
@property (nonatomic) FoodInfo *selectedFoodDetail;
@property (nonatomic) FoodCellTableViewCell *selectedCell;
//@property (nonatomic) NSMutableArray *addedItems;
@property (nonatomic) NSArray *allSavedItems;
@property (nonatomic) NSURLSessionDataTask *task;
@end

@implementation FoodTableViewController

-(void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefault objectForKey:[NSString stringWithFormat:@"allSavedItems"]];
    self.allSavedItems = [NSKeyedUnarchiver unarchiveObjectWithData: outData];
    NSLog(@"start");
    for (int i = 0; i < self.foodItems.count; i++) {
        FoodInfo *food = self.foodItems[i];
        food.added = NO;
    }
    for (int i = 0; i < self.allSavedItems.count; i++) {
        for (int j = 0; j < self.foodItems.count; j++) {
            FoodInfo *saved = self.allSavedItems[i];
            FoodInfo *food = self.foodItems[j];
            if (saved.foodNumber == food.foodNumber) {
                food.added = YES;
            }
        }
    }
    self.foodItems = [[self.foodItems sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
    [self.tableView reloadData];
    NSLog(@"finish");

    //self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (tableView == self.tableView) {
        return self.foodItems.count;
    } else {
        return self.searchResult.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodCellTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    NSArray *foodArray;
    // Configure the cell...
    if (tableView == self.tableView) {
        foodArray = self.foodItems;
    } else {    
        foodArray = self.searchResult;
    }
    //NSLog(@"%d", foodArray.count);
    FoodInfo *foodInfo = foodArray[indexPath.row];
    cell.foodName.text = foodInfo.foodName;
    
    //cell.foodName.numberOfLines = 0;
    
    cell.addButton.hidden = YES;
    if (foodInfo.added) {
        cell.addButton.enabled = NO;
    } else {
        cell.addButton.enabled = YES;
    }
    if (foodInfo.foodNumber == self.selectedItem.foodNumber) {
        cell.addButton.hidden = NO;
    }
    
    if (foodInfo.detailShowing) {
        cell.kcalLabel.hidden = NO;
        cell.proteinLabel.hidden = NO;
        cell.kcalLabel.text = [NSString stringWithFormat:@"%d", foodInfo.energyValue];
        cell.proteinLabel.text = [NSString stringWithFormat:@"%d", foodInfo.proteinValue];
        
    } else {
        cell.kcalLabel.hidden = YES;
        cell.proteinLabel.hidden = YES;
    }
    
    return cell;
}
- (IBAction)addClick:(id)sender {
//    [self loadApiForItem:self.selectedItem.foodNumber];
    if (!self.selectedItem.added) {
        self.selectedItem.added = YES;
        self.selectedCell.addButton.enabled = NO;
        //[self.selectedCell.addButton setTitle:@"Remove" forState:UIControlStateNormal];
        [self addItemToPersonalList:self.selectedItem];
    } else {
        //self.selectedItem.added = NO;
        //self.selectedCell.addButton.enabled = NO;
        //[self.selectedCell.addButton setTitle:@"Add" forState:UIControlStateNormal];
        //[self removeItemFromPersonalList];
    }
    //NSLog(@"%@", self.selectedItem.foodName);
//    [self updateSelectedItem];
//    [self performSelector:@selector(updateSelectedItem) withObject:nil afterDelay:3.0];
    //NSLog(@"%d", self.addedItems.count);
}

-(void)updateSelectedItem {
   
    
    self.selectedCell.foodName.text = self.selectedItem.foodName;
    self.selectedCell.kcalLabel.text = [NSString stringWithFormat:@"%d", self.selectedItem.energyValue];
    self.selectedCell.proteinLabel.text = [NSString stringWithFormat:@"%d", self.selectedItem.proteinValue];
    self.selectedCell.kcalLabel.hidden = NO;
    self.selectedCell.proteinLabel.hidden = NO;
    self.selectedCell.addButton.hidden = NO;
    //self.selectedCell.addButton.enabled = YES;
    [self.tableView reloadData];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchResult && self.searchResult.count > 1) {
        self.selectedItem = self.searchResult[indexPath.row];
    } else {
        self.selectedItem = self.foodItems[indexPath.row];    }
    
    self.selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    if (!self.selectedItem.detailShowing) {
        [self loadApiForItem:self.selectedItem.foodNumber];
    } else {
        [self updateSelectedItem];
    }
    
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    NSLog(@"log");
    self.selectedItem = nil;
    return YES;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    self.selectedCell.addButton.hidden = YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"foodName contains[c] %@", searchText];
    self.searchResult = [self.foodItems filteredArrayUsingPredicate:predicate];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchResult = nil;
    NSLog(@"cancel");
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


-(void)addItemToPersonalList:(FoodInfo*)info {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefaults objectForKey:@"allSavedItems"];
    
    NSMutableArray *items = [[NSKeyedUnarchiver unarchiveObjectWithData: outData] mutableCopy];
    if (!items) {
        NSLog(@"array init");
        items = [[NSMutableArray alloc] init];
    }
    //forloop kolla om objektet redan finns i userdefaults
    /*for (int i = 0; i < items.count; i++) {
        FoodInfo *foodInfo = items[i];
        if (foodInfo.foodNumber == self.selectedItem.foodNumber) {
            NSLog(@"food already saved");
            return;
        }
    }*/
    if (!items) {
        NSLog(@"nil");
    }
    [items addObject:info];
    NSLog(@"%d", items.count);
    NSLog(@"%@", self.selectedItem.foodName);
    NSData *inData = [NSKeyedArchiver archivedDataWithRootObject:items];
    [userDefaults setObject:inData forKey:@"allSavedItems"];
    [userDefaults synchronize];
}

-(void)removeItemFromPersonalList {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *outData = [userDefaults objectForKey:@"allSavedItems"];
    NSMutableArray *items = [[NSKeyedUnarchiver unarchiveObjectWithData: outData] mutableCopy];;
    for (int i = 0; i < items.count; i++) {
        FoodInfo *foodInfo = items[i];
        if (foodInfo.foodNumber == self.selectedItem.foodNumber) {
            [items removeObjectAtIndex:i];
        }
    }
    //[items removeObjectIdenticalTo:self.selectedItem];
    NSLog(@"%d", items.count);
    NSData *inData = [NSKeyedArchiver archivedDataWithRootObject:items];
    [userDefaults setObject:inData forKey:@"allSavedItems"];
    [userDefaults synchronize];
}

- (IBAction)goToPersonalList:(id)sender {
    [self performSegueWithIdentifier:@"segueToSaved" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueToSaved"]) {
//        SavedTableViewController *savedTableView = [segue destinationViewController];
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        savedTableView.savedList = [[userDefaults objectForKey:@"allSavedItems"] mutableCopy];

    }

}

-(void)loadApiForItem:(NSInteger) number {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", number]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    if (self.task) {
        [self.task cancel];
    }
    self.task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (error) {
                                          NSLog(@"Error in response: %@", error);
                                          return;
                                      }
                                      
                                      NSError *parsingError = nil;
                                      NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                                      NSLog(@"%d", root.count);
                                      
                                      if (!parsingError) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (root.count > 0) {
                                                  for (int i = 0; i < self.foodItems.count; i++) {
                                                      FoodInfo *foodInfo = self.foodItems[i];
                                                      if (foodInfo.foodNumber == number) {
                                                          foodInfo.energyValue = [root[@"nutrientValues"][@"energyKcal"] integerValue];
                                                          foodInfo.proteinValue = [root[@"nutrientValues"][@"protein"] integerValue];
                                                          foodInfo.carbValue = [root[@"nutrientValues"][@"carbohydrates"] integerValue];
                                                          foodInfo.detailShowing = YES;
                                                      }
                                                  }
                                                      //self.selectedFoodDetail = [[FoodInfo alloc] init];
                                                      self.selectedItem.foodName = root[@"name"];
                                                      self.selectedItem.foodNumber = [root[@"number"] integerValue];
                                                      self.selectedItem.energyValue = [root[@"nutrientValues"][@"energyKcal"] integerValue];
                                                      self.selectedItem.proteinValue = [root[@"nutrientValues"][@"protein"] integerValue];
                                                      
                                                  
                                                  //loop through first 20 items and get kcal and protein
                                                  //save in foodInfo
                                              } else {
                                                  NSLog(@"No topics found"  ) ;
                                              }
                                              self.selectedItem.detailShowing = YES;
                                              [self updateSelectedItem];
                                          });
                                      } else {
                                          NSLog(@"Couldn't parse json: %@", parsingError);
                                      }
                                      self.task = nil;
                                  }];
    [self.task resume];
    
}


@end













