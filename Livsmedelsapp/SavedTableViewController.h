//
//  SavedTableViewController.h
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-07.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedTableViewController : UITableViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic) NSMutableArray *savedList;
@end
