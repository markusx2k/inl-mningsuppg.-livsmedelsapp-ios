//
//  DetailViewController.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *kcalLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitaminCLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitaminDLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitaminELabel;

@end

@implementation DetailViewController

-(void)updateLabels {
    self.foodName.text = self.foodInfo.foodName;
    self.kcalLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.energyValue];
    self.proteinLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.proteinValue];
    self.fatLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.fatValue];
    self.carbLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.carbValue];
    self.vitaminCLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.vitC];
    self.vitaminDLabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.vitD];
    self.vitaminELabel.text = [NSString stringWithFormat:@"%d", self.foodInfo.vitE];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadApiForItem:self.foodInfo.foodNumber];
    NSLog(@"%@", self.foodInfo.foodName);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadApiForItem:(NSInteger) number {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", number]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
 
                                      if (error) {
                                          NSLog(@"Error in response: %@", error);
                                          return;
                                      }
 
                                      NSError *parsingError = nil;
                                      NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parsingError];
                                      NSLog(@"%d", root.count);
 
                                      if (!parsingError) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              if (root.count > 0) {
                                                  self.foodInfo.energyValue = [root[@"nutrientValues"][@"energyKcal"] integerValue];
                                                  self.foodInfo.proteinValue = [root[@"nutrientValues"][@"protein"] integerValue];
                                                  self.foodInfo.carbValue = [root[@"nutrientValues"][@"carbohydrates"] integerValue];
                                                  self.foodInfo.fatValue = [root[@"nutrientValues"][@"fat"] integerValue];
                                                  self.foodInfo.vitC = [root[@"nutrientValues"][@"vitaminC"] integerValue];
                                                  self.foodInfo.vitD = [root[@"nutrientValues"][@"vitaminD"] integerValue];
                                                  self.foodInfo.vitE = [root[@"nutrientValues"][@"vitaminE"] integerValue];
                                              } else {
                                                  NSLog(@"No topics found"  ) ;
                                              }
                                          });
                                      } else {
                                          NSLog(@"Couldn't parse json: %@", parsingError);
                                      }
                                      [self updateLabels];
                                  }];
    [task resume];
}


@end
