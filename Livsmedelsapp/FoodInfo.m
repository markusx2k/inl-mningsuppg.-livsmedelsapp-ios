//
//  FoodInfo.m
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-02-25.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import "FoodInfo.h"

@implementation FoodInfo

- (NSComparisonResult)compare:(FoodInfo *)otherObject {
    return [self.foodName compare:otherObject.foodName];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.foodName forKey:@"foodName"];
    [encoder encodeInteger:self.foodNumber forKey:@"foodNumber"];
    [encoder encodeInteger:self.energyValue forKey:@"energyValue"];
    [encoder encodeInteger:self.proteinValue forKey:@"proteinValue"];
    
    [encoder encodeObject:self.cachePath forKey:@"cachePath"];

    [encoder encodeBool:[self added] forKey:@"added"];
    [encoder encodeBool:[self detailShowing] forKey:@"detailShowing"];
    [encoder encodeBool:[self hasPicture] forKey:@"hasPicture"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.foodName = [decoder decodeObjectForKey:@"foodName"];
        self.foodNumber = [decoder decodeIntegerForKey:@"foodNumber"];
        self.energyValue = [decoder decodeIntegerForKey:@"energyValue"];
        self.proteinValue = [decoder decodeIntegerForKey:@"proteinValue"];
        
        self.cachePath = [decoder decodeObjectForKey:@"cachePath"];
        
        self.added = [decoder decodeBoolForKey:@"added"];
        self.detailShowing = [decoder decodeBoolForKey:@"detailShowing"];
        self.hasPicture = [decoder decodeBoolForKey:@"hasPicture"];
        
    }
    return self;
}

@end
