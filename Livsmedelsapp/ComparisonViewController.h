//
//  ComparisonViewController.h
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-03-16.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>
#import "FoodInfo.h"

@interface ComparisonViewController : UIViewController <GKBarGraphDataSource>
@property (nonatomic) FoodInfo *f1;
@property (nonatomic) FoodInfo *f2;
@end
