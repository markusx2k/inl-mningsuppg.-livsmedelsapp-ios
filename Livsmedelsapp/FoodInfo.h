//
//  FoodInfo.h
//  Livsmedelsapp
//
//  Created by IT-Högskolan on 2015-02-25.
//  Copyright (c) 2015 Markus H. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodInfo : NSObject
@property (nonatomic) NSString *foodName;
@property (nonatomic) NSInteger foodNumber;
@property (nonatomic) NSInteger energyValue;
@property (nonatomic) NSInteger proteinValue;

@property (nonatomic) NSString *cachePath;

@property (nonatomic) BOOL added;
@property (nonatomic) BOOL detailShowing;
@property (nonatomic) BOOL hasPicture;

//no save
@property (nonatomic) BOOL comparing;
@property (nonatomic) NSInteger carbValue;
@property (nonatomic) NSInteger fatValue;
@property (nonatomic) NSInteger vitC;
@property (nonatomic) NSInteger vitD;
@property (nonatomic) NSInteger vitE;

@end
